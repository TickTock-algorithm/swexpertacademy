/**등산로 조성
 * https://www.swexpertacademy.com/main/code/problem/problemDetail.do?contestProbId=AV5PoOKKAPIDFAUq
 * 시간 : 50개 테스트케이스를 합쳐서 C++의 경우 3초 / Java의 경우 3초
 * 메모리 : 힙, 정적 메모리 합쳐서 256MB 이내, 스택 메모리 1MB 이내
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MAX_DIG 1

int T, N, K;
int *map, start, result;
int find(int *map, int r, int c, int dig, int len);

int move(int *map, int r, int c, int dig, int len, int m){
    int res=len;
    int *tmp_map=(int*)malloc(sizeof(int)*N*N);
    memcpy(tmp_map, map, sizeof(int)*N*N);
    if(*(tmp_map+r*N+c)>*(tmp_map+r*N+c+m)){
        *(tmp_map+r*N+c)=100;
        res=find(tmp_map, r+m/N, c+m%N, dig, len);
    }else if(dig>0 && (*(tmp_map+r*N+c)>(*(tmp_map+r*N+c+m)-K))){
        *(tmp_map+r*N+c+m)=*(tmp_map+r*N+c)-1;
        *(tmp_map+r*N+c)=100;
        res=find(tmp_map, r+m/N, c+m%N, dig-1, len);
    }
    free(tmp_map);
    return res;
}

int find(int *map, int r, int c, int dig, int len){
    int max=0, tmp;
    len++;
    if(r>0){
        tmp=move(map, r, c, dig, len, -N);
        max=tmp>max?tmp:max;
    }
    if(c>0){
        tmp=move(map, r, c, dig, len, -1);
        max=tmp>max?tmp:max;
    }
    if(r<N-1){
        tmp=move(map, r, c, dig, len, N);
        max=tmp>max?tmp:max;
    }
    if(c<N-1){
        tmp=move(map, r, c, dig, len, 1);
        max=tmp>max?tmp:max;
    }
    return max;
}

int solve(int *map, int start){
    int max=0, tmp;
    for(int i=0; i<N*N; i++)
        if(*(map+i)==start){
            tmp=find(map, i/N, i%N, MAX_DIG, 0);
            max=tmp>max?tmp:max;
        }
    return max;
}

int main(){
    scanf("%d", &T);
    for(int _=1; _<=T; _++){
        scanf("%d %d", &N, &K);
        map=(int*)malloc(sizeof(int)*N*N);
        for(int i=0; i<N*N; i++){
            scanf("%d", map+i);
            start=*(map+i)>start?*(map+i):start;
        }
        printf("#%d %d\n", _, solve(map, start));
        start=0;
        free(map);
    }
}