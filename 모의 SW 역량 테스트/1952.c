/**수영장
 * https://www.swexpertacademy.com/main/code/problem/problemDetail.do?contestProbId=AV5PpFQaAQMDFAUq
 * 시간 : 50개 테스트케이스를 합쳐서 C++의 경우 3초 / Java의 경우 3초
 * 메모리 : 힙, 정적 메모리 합쳐서 256MB 이내, 스택 메모리 1MB 이내
 */

#include<stdio.h>

int T, P[4], M[12];

int solve(int month, int total){
    if(total>P[3])
        return P[3];
    if(month>=12)
        return total;
        
    int min=P[3], tmp;
    
    tmp=solve(month+1, total+(M[month]*P[0]<P[1]?M[month]*P[0]:P[1]));
    min=min<tmp?min:tmp;
    tmp=solve(month+3, total+P[2]);
    min=min<tmp?min:tmp;

    return min;
}

int main(){
    scanf("%d", &T);
    for(int tc=1; tc<=T; tc++){
        for(int i=0; i<4; i++)
            scanf("%d", P+i);
        for(int i=0; i<12; i++)
            scanf("%d", M+i);
        printf("#%d %d\n", tc, solve(0, 0));
    }
}