/**탈주범 검거
 * https://www.swexpertacademy.com/main/code/problem/problemDetail.do?contestProbId=AV5PpLlKAQ4DFAUq
 * 시간 : 50개 테스트케이스를 합쳐서 C++의 경우 1초 / Java의 경우 1초
 * 메모리 : 힙, 정적 메모리 합쳐서 256MB 이내, 스택 메모리 1MB 이내
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define BASE 10

int T, N, M, R, C, L;
int *MAP, *thif;
int PIPE[7][4]={
    {-1, 1, -1, 1},
    {0, 0, -1, 1},
    {-1, 1, 0, 0},
    {0, 1, -1, 0},
    {0, 1, 0, 1},
    {-1, 0, 0, 1},
    {-1, 0, -1, 0}
};

void move(int r, int c, int time){
    int *p=MAP+r*M+c;
    if(*p>0){
        int type=*p-1, *tp;
        *(thif+r*M+c)=1;
        if(c>0){
            tp=(p+PIPE[type][0]);
            if(*tp>=0 && PIPE[*tp-1][1] && !*(thif+r*M+c+PIPE[type][0]))
                *(thif+r*M+c+PIPE[type][0])=BASE+time;
        }
        if(c<M-1){
            tp=(p+PIPE[type][1]);
            if(*tp>=0 && PIPE[*tp-1][0] && !*(thif+r*M+c+PIPE[type][1]))
                *(thif+r*M+c+PIPE[type][1])=BASE+time;
        }
        if(r>0){
            tp=(p+PIPE[type][2]*M);
            if(*tp>=0 && PIPE[*tp-1][3] && !*(thif+r*M+c+PIPE[type][2]*M))
                *(thif+r*M+c+PIPE[type][2]*M)=BASE+time;
        }
        if(r<N-1){
            tp=(p+PIPE[type][3]*M);
            if(*tp>=0 && PIPE[*tp-1][2] && !*(thif+r*M+c+PIPE[type][3]*M))
                *(thif+r*M+c+PIPE[type][3]*M)=BASE+time;
        }
    }
}

int solve(int r, int c, int time){
    *(thif+r*M+c)=BASE;
    for(int t=0; t<time; t++){
        for(int i=0; i<N*M; i++){
            if(*(thif+i)==BASE+t){
                move(i/M, i%M, t+1);
            }
        }
    }
    int res=0;
    for(int i=0; i<N*M; i++)
        if(*(thif+i)==1)
            res++;
    return res;
}

int main(){
    scanf("%d", &T);
    for(int tc=1; tc<=T; tc++){
        scanf("%d %d %d %d %d", &N, &M, &R, &C, &L);
        MAP=(int*)malloc(sizeof(int)*N*M);
        thif=(int*)malloc(sizeof(int)*N*M);
        memset(thif, 0, sizeof(int)*M*N);
        for(int i=0; i<N*M; i++)
            scanf("%d", MAP+i);
        printf("#%d %d\n", tc, solve(R, C, L));
        free(MAP);
    }
}