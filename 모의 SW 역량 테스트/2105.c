/**디저트 카페
 * https://www.swexpertacademy.com/main/code/problem/problemDetail.do?contestProbId=AV5VwAr6APYDFAWu
 * 시간 : 50개 테스트케이스를 합쳐서 C++의 경우 3초 / Java의 경우 3초
 * 메모리 : 힙, 정적 메모리 합쳐서 256MB 이내, 스택 메모리 1MB 이내
 */

#include<stdio.h>
#include<stdlib.h>

int T, N;
int *map;

int check(int* path, int len){
    for(int i=0; i<len; i++)
        for(int g=i+1; g<len; g++)
            if(path[i]==path[g])
                return 1;
    return 0;
}

int solve(){
    int *path=(int*)malloc(sizeof(int)*N*2);
    int max=0;

    for(int i=0; i<N*N; i++){
        int len=0;
        int lmax=i%N, rmax=N-1-i%N, lrmax=N-1-i/N;

        for(int l=1; l<=lmax; l++){
            len=l-1;
            path[len++]=*(map+i+l*(N-1));
            for(int r=1; r<=rmax; r++){
                if(l+r>lrmax)
                    break;
                
                len=l+r-1;
                path[len++]=*(map+i+l*(N-1)+r*(N+1));
                
                if(check(path, len))
                    break;
                
                int *p=map+i+l*(N-1)+r*(N+1);
                for(int rl=1; rl<=l; rl++){
                    p-=(N-1);
                    path[len++]=*p;
                }
                for(int rr=1; rr<=r; rr++){
                    p-=(N+1);
                    path[len++]=*p;
                }
                
                if(check(path, len))
                    continue;
                max=len>max?len:max;
            }
        }
    }
    free(path);
    return max?max:-1;
}

int main(){
    scanf("%d", &T);
    for(int tc=1; tc<=T; tc++){
        scanf("%d", &N);
        map=(int*)malloc(sizeof(int)*N*N);
        for(int i=0; i<N*N; i++)
            scanf("%d", map+i);
        printf("#%d %d\n", tc, solve());
        free(map);
    }
    return 0;
}
