/**보호 필름
 * https://www.swexpertacademy.com/main/code/problem/problemDetail.do?contestProbId=AV5V1SYKAaUDFAWu
 * 시간 : 50개 테스트케이스를 합쳐서 C++의 경우 5초 / Java의 경우 5초
 * 메모리 : 힙, 정적 메모리 합쳐서 256MB 이내, 스택 메모리 1MB 이내
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int T, D, W, K;
int *film;

int check(int *chemi){
    for(int i=0; i<W; i++){
        int k=0, prev=-1;
        for(int g=0; g<D; g++){
            if(prev!=((chemi[g]>=0)?chemi[g]:*(film+g*W+i))){
                prev=(chemi[g]>=0)?chemi[g]:*(film+g*W+i);
                k=1;
            }else
                k++;
            if(k>=K)
                break;
        }
        if(k>=K)
            continue;
        return 1;
    }
    return 0;
}

int func(int *chemi, int idx, int count){
    if(count>=K)
        return K;
    
    if(check(chemi)){
        int min=K, tmp;
        for(int i=1; i>=-1 && idx<D; i--){
            chemi[idx]=i;
            tmp=func(chemi, idx+1, count+(i>=0?1:0));
            min=min>tmp?tmp:min;
        }
        return min;
    }else
        return count;
}

int solve(){
    int *chemi=(int*)malloc(sizeof(int)*D);
    memset(chemi, -1, sizeof(int)*D);
    int res=func(chemi, 0, 0);
    free(chemi);
    return res;
}

int main(){
    scanf("%d", &T);
    for(int tc=1; tc<=T; tc++){
        scanf("%d %d %d", &D, &W, &K);
        film=(int*)malloc(sizeof(int)*D*W);
        for(int i=0; i<D*W; i++)
            scanf("%d", film+i);
        printf("#%d %d\n", tc, solve());
        free(film);
    }
}