/**벌꿀채취
 * https://www.swexpertacademy.com/main/code/problem/problemDetail.do?contestProbId=AV5V4A46AdIDFAWu
 * 시간 : 50개 테스트케이스를 합쳐서 C++의 경우 3초 / Java의 경우 3초
 * 메모리 : 힙, 정적 메모리 합쳐서 256MB 이내, 스택 메모리 1MB 이내
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int T, N, M, C;
int *map;

int get_result(int* profit_map){
    int max=0, tmp;
    for(int i=0; i<N*N; i++){
        for(int g=i+M; *(profit_map+i) && g<N*N; g++){
            tmp=*(profit_map+i)+*(profit_map+g);
            max=max>tmp?max:tmp;
        }
    }
    return max;
}

int get_profit(int *p, int idx, int *select, int len, int state){
    if(state>C)
        return 0;
    if(idx<M){
        int max=get_profit(p, idx+1, select, len, state);
        select[len++]=*(p+idx);
        int tmp=get_profit(p, idx+1, select, len, state+*(p+idx));
        max=max>tmp?max:tmp;
        return max;
    }else{
        int sum=0;
        for(int i=0; i<len; i++)
            sum+=*(select+i)*(*(select+i));
        return sum;
    }
}

int solve(){
    int *profit_map=(int*)malloc(sizeof(int)*N*N);
    memset(profit_map, 0, sizeof(int)*N*N);
    int *select=(int*)malloc(sizeof(int)*M);
    int *a, *b;
    for(int r=0; r<N; r++){
        for(int i=0, len=N-M+1; i<len; i++)
            *(profit_map+r*N+i)=get_profit(map+r*N+i, 0, select, 0, 0);
    }
    
    int res=get_result(profit_map);
    free(select);
    free(profit_map);
    return res;
}

int main(){
    scanf("%d", &T);
    for(int tc=1; tc<=T; tc++){
        scanf("%d %d %d", &N, &M, &C);
        map=(int*)malloc(sizeof(int)*N*N);
        for(int i=0; i<N*N; i++)
            scanf("%d", map+i);
        printf("#%d %d\n", tc, solve());
        free(map);
    }
}