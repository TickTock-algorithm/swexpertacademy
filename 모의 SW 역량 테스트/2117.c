/**홈 방범 서비스
 * https://www.swexpertacademy.com/main/code/problem/problemDetail.do?contestProbId=AV5V61LqAf8DFAWu&
 * 시간 : 50개 테스트케이스를 합쳐서 C++의 경우 3초 / Java의 경우 3초
 * 메모리 : 힙, 정적 메모리 합쳐서 256MB 이내, 스택 메모리 1MB 이내
 */
#include<stdio.h>
#include<stdlib.h>

int T, N, M;
int *map;

struct house{
    int r;
    int c;
};

struct house* find_house(int count){
    struct house *houses=(struct house*)malloc(sizeof(struct house)*count);
    int len=0;
    for(int i=0; i<N*N && len<count; i++){
        if(*(map+i)){
            houses[len].r=i/N;
            houses[len++].c=i%N;
        }
    }
    return houses;
}

int solve(int count){
    struct house *houses=find_house(count);
    int max=0;
    for(int r=0; r<N; r++){
        for(int c=0; c<N; c++){
            for(int k=1, tmp=0; tmp<count; k++){
                tmp=0;
                for(int i=0; i<count; i++){
                    if(k>abs(houses[i].r-r)+abs(houses[i].c-c))
                        tmp++;
                }
                if(k*k+(k-1)*(k-1)<=tmp*M)
                    max=max>tmp?max:tmp;
            }
        }
    }
    free(houses);
    return max;
}

int main(){
    scanf("%d", &T);
    for(int tc=1; tc<=T; tc++){
        scanf("%d %d", &N, &M);
		map=(int*)malloc(sizeof(int)*N*N);
        int count=0;
        for(int i=0; i<N*N; i++){
            scanf("%d", map+i);
            count+=*(map+i);
        }
        
        printf("#%d %d\n", tc, solve(count));
        free(map);
        
    }
}