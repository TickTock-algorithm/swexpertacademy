/**미생물 격리
 * https://www.swexpertacademy.com/main/code/problem/problemDetail.do?contestProbId=AV597vbqAH0DFAVl
 * 시간 : 50개 테스트케이스를 합쳐서 C++의 경우 5초 / Java의 경우 5초
 * 메모리 : 힙, 정적 메모리 합쳐서 256MB 이내, 스택 메모리 1MB 이내
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int T, N, M, K;

struct bug{
    int r;
    int c;
    int n;
    int d;
};

struct bug *bugs;

void check(){
    for(int i=0; i<K; i++){
        if(!bugs[i].n)
            continue;
        if(bugs[i].r==0 || bugs[i].c==0 || bugs[i].r==N-1 || bugs[i].c==N-1){
            bugs[i].n/=2;
            bugs[i].d=((--bugs[i].d)%2)?bugs[i].d:(bugs[i].d+2);
        }else{
            struct bug *maxbug=bugs+i;
            int max=maxbug->n;
            for(int g=i+1; g<K; g++){
                if(bugs[i].r==bugs[g].r && bugs[i].c==bugs[g].c){
                    if(max<bugs[g].n){
                        max=bugs[g].n;
                        bugs[g].n+=maxbug->n;
                        maxbug->n=0;
                        maxbug=bugs+g;
                    }else{
                        maxbug->n+=bugs[g].n;
                        bugs[g].n=0;
                    }
                }
            }
        }
    }
}
void move(){
    for(int i=0; i<K; i++){
        if(!bugs[i].n)
            continue;
        if(bugs[i].d>2)
            bugs[i].c+=(bugs[i].d%2?-1:1);
        else
            bugs[i].r+=(bugs[i].d%2?-1:1);
    }
}
int solve(){
    for(int _=0; _<M; _++){
        move();
        check();
    }
    int sum=0;
    for(int i=0; i<K; i++)
        sum+=bugs[i].n;
    return sum;
}

int main(){
    scanf("%d", &T);
    for(int tc=1; tc<=T; tc++){
        scanf("%d %d %d", &N, &M, &K);
        bugs=(struct bug*)malloc(sizeof(struct bug)*K);
        for(int i=0; i<K; i++)
            scanf("%d %d %d %d", &bugs[i].r, &bugs[i].c, &bugs[i].n, &bugs[i].d);
        printf("#%d %d\n", tc, solve());
        free(bugs);
    }
}