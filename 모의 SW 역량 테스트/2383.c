/**점심 식사시간
 * https://www.swexpertacademy.com/main/code/problem/problemDetail.do?contestProbId=AV5-BEE6AK0DFAVl
 * 시간 : 50개 테스트케이스를 합쳐서 C++의 경우 3초 / Java의 경우 3초
 * 메모리 : 힙, 정적 메모리 합쳐서 256MB 이내, 스택 메모리 1MB 이내
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define DOOR_NUM 2
#define MAX_QUEUE 3

#define STATE_WALK 0 
#define STATE_WAIT 1
#define STATE_DOWN 2
#define STATE_DONE 3

int T, N;
int *map;
int plen=0;

struct person{
    int r, c;
    int to_door[DOOR_NUM];
    int state;
    int door;
    int time;
} *person;

struct door{
    int r, c;
    int time;
    int wait_num;
} *door;

int move(){
    int check=0;

    for(int i=0; i<plen; i++){
        struct person *p=person+i;
        if(p->state==STATE_DOWN){
            if(--p->time){
                check=1;
            }else{
                p->state=STATE_DONE;
                door[p->door].wait_num--;
            }
        }
    }

    for(int i=0; i<plen; i++){
        struct person *p=person+i;
        switch(p->state){
            case STATE_WALK:
                check=1;
                if(!(--p->time)){
                    p->state=STATE_WAIT;
                }
                break;
            case STATE_WAIT:
                check=1;
                if(door[p->door].wait_num<MAX_QUEUE){
                    p->state=STATE_DOWN;
                    p->time=door[p->door].time;
                    door[p->door].wait_num++;
                }
                break;
        }
    }
    

    return check;
}
void maybe_best(){
    for(int i=0; i<plen; i++){
        struct person *p=person+i;
        p->time=(unsigned int)~0>>1;
        for(int g=0; g<DOOR_NUM; g++){
            int tmp=p->to_door[g]+door[g].time;
            if(p->time>tmp){
                p->time=tmp;
                p->door=g;
            }
        }
    }
}
void init(int d){
    for(int g=0; g<DOOR_NUM; g++)
        door[g].wait_num=0;
    for(int g=0; g<plen; g++){
        struct person *p=person+g;
        p->state=STATE_WALK;
        p->door=(d&1<<g)?1:0;
        p->time=p->to_door[p->door];
    }
}

int solve(){
    init(0);
    maybe_best();

    int time=1;
    while(move())
        time++;

    int min=time;
    for(int i=0; i<(1<<plen); i++){
        init(i);
        time=1;
        while(time<min && move())
            time++;
        min=min>time?time:min;
    }

    return min;
}

int main(){
    scanf("%d", &T);
    
    door=(struct door*)malloc(sizeof(struct door)*DOOR_NUM);
    for(int tc=1; tc<=T; tc++){
        scanf("%d", &N);
        map=(int*)malloc(sizeof(int)*N*N);
        
        int dlen=0;
        for(int i=0; i<N*N; i++){
            scanf("%d", map+i);
            if(*(map+i)==1)
                plen++;
        }
        person=(struct person*)malloc(sizeof(struct person)*plen);
        plen=0;

        for(int i=0; i<N*N; i++){
            if(*(map+i)>1){
                door[dlen].r=i/N;
                door[dlen].c=i%N;
                door[dlen++].time=*(map+i);
            }else if(*(map+i)){
                person[plen].r=i/N;
                person[plen++].c=i%N;
            }
        }
        for(int i=0; i<plen; i++)
            for(int g=0; g<dlen; g++)
                person[i].to_door[g]=abs(person[i].r-door[g].r)+abs(person[i].c-door[g].c);

        free(map);
        printf("#%d %d\n", tc, solve());
        free(person);
    }
    free(door);
}