/**차량 정비소
 * https://www.swexpertacademy.com/main/code/problem/problemDetail.do?contestProbId=AV6c6bgaIuoDFAXy
 * 시간 : 50개 테스트케이스를 합쳐서 C++의 경우 3초 / Java의 경우 3초
 * 메모리 : 힙, 정적 메모리 합쳐서 256MB 이내, 스택 메모리 1MB 이내
 */

#include<stdio.h>
#include<stdlib.h>

#define STATE_WAIT 1
#define STATE_WORK 2

#define STATE_WAIT_RECEPTION 1
#define STATE_RECETION 2
#define STATE_WAIT_REPAIR 3
#define STATE_REPAIR 4
#define STATE_DONE 5

struct customer{
    int state;
    int time;
    int visited[2];
} *t, **q;
struct desk{
    int state;
    int time;
    struct customer *customer;
} *a, *b;


int T, N, M, K, A, B;
int qs=0, qe=0;

void work(){
    for(int i=0; i<N; i++){
        if(a[i].state==STATE_WORK){
            if(a[i].customer->time<=0){
                a[i].customer->state=STATE_WAIT_REPAIR;
                a[i].state=STATE_WAIT;
                q[qe++]=a[i].customer;
            }else
                a[i].customer->time--;
        }
    }
    for(int i=0; i<M; i++){
        if(b[i].state==STATE_WORK){
            if(b[i].customer->time<=0){
                b[i].customer->state=STATE_DONE;
                b[i].state=STATE_WAIT;
            }else
                b[i].customer->time--;
        }
    }
}

int wait(){
    int done=0;
    for(int i=0; i<K; i++){
        if(t[i].state==STATE_WAIT_RECEPTION){
            if(t[i].time<=0){
                for(int g=0; g<N; g++){
                    if(a[g].state==STATE_WAIT){
                        t[i].state=STATE_RECETION;
                        t[i].visited[0]=g;
                        t[i].time=a[g].time-1;
                        a[g].state=STATE_WORK;
                        a[g].customer=t+i;
                        break;
                    }
                }
            }else
                t[i].time--;
        }
    }

    for(int i=0; i<M && qs<qe; i++)
        if(b[i].state==STATE_WAIT){
            q[qs]->state=STATE_REPAIR;
            q[qs]->visited[1]=i;
            q[qs]->time=b[i].time-1;
            b[i].state=STATE_WORK;
            b[i].customer=q[qs];
            qs++;
            done++;
        }
    return done;
}

int solve(){
    int res=0;
    int count=0;
    while(res<K){
        work();
        res+=wait();     
        count++;

    }
    res=0;
    A--;
    B--;
    for(int i=0; i<K; i++){
        if(t[i].visited[0]==A && t[i].visited[1]==B)
            res+=i+1;
    }
    return res?res:-1;
}

int main(){
    scanf("%d", &T);
    for(int tc=1; tc<=T; tc++){
        scanf("%d %d %d %d %d", &N, &M, &K, &A, &B);

        a=(struct desk*)malloc(sizeof(struct desk)*N);
        b=(struct desk*)malloc(sizeof(struct desk)*M);
        t=(struct customer*)malloc(sizeof(struct customer)*K);
        q=(struct customer**)malloc(sizeof(struct customer*)*K);
        
        for(int i=0; i<N; i++){
            scanf("%d", &a[i].time);
            a[i].state=STATE_WAIT;
        }
        for(int i=0; i<M; i++){
            scanf("%d", &b[i].time);
            b[i].state=STATE_WAIT;
        }
        for(int i=0; i<K; i++){
            scanf("%d", &t[i].time);
            t[i].state=STATE_WAIT_RECEPTION;
        }
        qe=qs=0;

        printf("#%d %d\n", tc, solve());

        free(a);
        free(b);
        free(t);
        free(q);
    }
    
}