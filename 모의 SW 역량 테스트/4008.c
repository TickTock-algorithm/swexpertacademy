/**숫자 만들기
 * https://www.swexpertacademy.com/main/code/problem/problemDetail.do?contestProbId=AWIeRZV6kBUDFAVH
 * 시간 : 50개 테스트케이스를 합쳐서 C++의 경우 5초 / Java의 경우 5초
 * 메모리 : 힙, 정적 메모리 합쳐서 256MB 이내, 스택 메모리 1MB 이내
 */

#include<stdio.h>
#include<stdlib.h>

int T, N;
int oper[4], *num;
int min, max;

void calc(int oper[4], int idx, int result){
    if(idx>=N){
        min=min>result?result:min;
        max=max>result?max:result;
        return;
    }
    int tmp;
    for(int i=0; i<4; i++){
        if(oper[i]>0){
            switch(i){
                case 0:
                    tmp=result+num[idx];
                    break;
                case 1:
                    tmp=result-num[idx];
                    break;
                case 2:
                    tmp=result*num[idx];
                    break;
                case 3:
                    tmp=result/num[idx];
                    break;
            }
            oper[i]--;
            calc(oper, idx+1, tmp);
            oper[i]++;
        }
    }
}

int solve(){
    min=100000000;
    max=-100000000;
    calc(oper, 1, num[0]);
    return max-min;
}

int main(){
    scanf("%d", &T);
    
    for(int tc=1; tc<=T; tc++){
        scanf("%d", &N);
        for(int i=0; i<4; i++)
            scanf("%d", oper+i);
        num=(int*)malloc(sizeof(int)*N);
        for(int i=0; i<N; i++)
            scanf("%d", num+i);
        printf("#%d %d\n", tc, solve());
        free(num);
    }
}