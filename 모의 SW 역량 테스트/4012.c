/**요리사
 * https://www.swexpertacademy.com/main/code/problem/problemDetail.do?contestProbId=AWIeUtVakTMDFAVH
 * 시간 : 50개 테스트케이스를 합쳐서 C++의 경우 5초 / Java의 경우 5초
 * 메모리 : 힙, 정적 메모리 합쳐서 256MB 이내, 스택 메모리 1MB 이내
 */

#include <stdio.h>
#include <stdlib.h>

int T, N;
int *table;

int calc(int idx, int result, int select, int count) {
    if (idx - count > N / 2 || count > N / 2)  //반씩 나눠지지 않음
        return ~0u >> 1;
    if (idx == N) return abs(result);
    
    int tmp1 = 0, tmp2 = 0;
    for (int i = 0; i < idx; i++)
        if (select & (1 << i))
            tmp1 += *(table + i * N + idx);
        else
            tmp2 += *(table + i * N + idx);

    int res2 = calc(idx + 1, result - tmp2, select, count);
    if (!res2) return 0;
    select |= (1 << idx);
    int res1 = calc(idx + 1, result + tmp1, select, count + 1);
    return !res1 ? 0 : (res1 < res2 ? res1 : res2);
}

int solve() { return calc(0, 0, 1, 1); }

int main() {
    scanf("%d", &T);
    for (int tc = 1; tc <= T; tc++) {
        scanf("%d", &N);
        table = (int *)malloc(sizeof(int) * N * N);
        for (int i = 0; i < N * N; i++) scanf("%d", table + i);
        for (int i = 0; i < N; i++)
            for (int g = i + 1; g < N; g++)
                *(table + i * N + g) += *(table + g * N + i);

        printf("#%d %d\n", tc, solve());
        free(table);
    }
}