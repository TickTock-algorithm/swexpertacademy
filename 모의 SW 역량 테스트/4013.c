/**특이한 자석
 * https://www.swexpertacademy.com/main/code/problem/problemDetail.do?contestProbId=AWIeW7FakkUDFAVH
 * 시간 : 50개 테스트케이스를 합쳐서 C++의 경우 5초 / Java의 경우 5초
 * 메모리 : 힙, 정적 메모리 합쳐서 256MB 이내, 스택 메모리 1MB 이내
 */

#include <stdio.h>
#include <stdlib.h>

#define NUM 4
#define GEAR 8

int T, K;
int gear[NUM];

void rotate(int num, int rot, int d) {
    if (num < 0 || num >= NUM) return;
    if (!d) {
        rotate(num - 1, -rot, -1);
        rotate(num + 1, -rot, 1);
    } else {
        if ((((gear[num] >> (GEAR / 4 * (d < 0 ? 1 : 3)))) & 1) !=
            (((gear[num - d] >> (GEAR / 4 * (d > 0 ? 1 : 3)))) & 1)) {
            rotate(num + d, -rot, d);
        } else
            return;
    }

    if (rot > 0) {
        gear[num] <<= 1;
        if (gear[num] & (1 << GEAR)) gear[num] |= 1;
        gear[num] &= ~(1 << GEAR);
    } else {
        int tmp = gear[num] & 1;
        gear[num] >>= 1;
        gear[num] |= (tmp << (GEAR - 1));
    }
}

int solve() {
    int num, rot;
    for (int i = 0; i < K; i++) {
        scanf("%d %d", &num, &rot);
        rotate(num - 1, rot, 0);
    }
    int result = 0;
    for (int i = 0; i < NUM; i++) result |= (gear[i] & 1) << i;
    return result;
}

int main() {
    scanf("%d", &T);
    for (int tc = 1; tc <= T; tc++) {
        scanf("%d", &K);
        for (int i = 0; i < NUM; i++) {
            int tmp;
            gear[i] = 0;
            for (int g = 0; g < GEAR; g++) {
                scanf("%d", &tmp);
                gear[i] |= tmp << g;
            }
        }
        printf("#%d %d\n", tc, solve());
    }
}