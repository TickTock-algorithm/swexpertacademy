/**활주로 건설
 * https://www.swexpertacademy.com/main/code/problem/problemDetail.do?contestProbId=AWIeW7FakkUDFAVH
 * 시간 : 50개 테스트케이스를 합쳐서 C++의 경우 5초 / Java의 경우 5초
 * 메모리 : 힙, 정적 메모리 합쳐서 256MB 이내, 스택 메모리 1MB 이내
 */

#include<stdio.h>
#include<stdlib.h>

#define HEIGHT 1

int T, N, X;
int *map;

int check(int *path, int step){
    int flat=1;
    for(int i=1; i<N; i++){
        int *p=path+i*step;
        switch(*(p)-*(p-step)){
            case 0:
                flat++;
                break;
            case HEIGHT:
                if(flat<X)
                    return 0;
                flat=1;
                break;
            case -HEIGHT:
                if(i+X>N)
                    return 0;
                for(int g=1; g<X; g++)
                    if(*(p+g*step)!=*(p+(g-1)*step))
                        return 0;
                flat=0;
                i+=X-1;
                
                break;
            default:
                return 0;
        }
    }
    return 1;
}

int solve(){
    int result=0;
    for(int i=0; i<N; i++){
        result+=check(map+i, N);
        result+=check(map+i*N, 1);
    }
    return result;
}

int main(){
    scanf("%d", &T);
    for(int tc=1; tc<=T; tc++){
        scanf("%d %d", &N, &X);
        map=(int*)malloc(sizeof(int)*N*N);
        for(int i=0; i<N*N; i++)
            scanf("%d", map+i);
        printf("#%d %d\n", tc, solve());
        free(map);
    }
}